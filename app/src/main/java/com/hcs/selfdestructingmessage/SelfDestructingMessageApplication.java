package com.hcs.selfdestructingmessage;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;

public class SelfDestructingMessageApplication extends Application {

    public static void updateParseInstallation(ParseUser user) {

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(ParseConstants.KEY_USERID, user.getObjectId());
        installation.saveInBackground();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, "NXKM3oqLar9D5lk8FBHX0cOP6DeIbBUUpCCcGl6Q", "WugxwLBxZKn8kpX3AkskobmRtH8GGeF6mJeDVuqg");
        PushService.setDefaultPushCallback(this, MainActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();


//        ParseObject testObject = new ParseObject("test object");
//        testObject.add("foo", "bar");
//        testObject.saveInBackground();
    }


}
