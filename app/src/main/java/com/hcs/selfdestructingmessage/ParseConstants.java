package com.hcs.selfdestructingmessage;

/**
 * Created by shivanshmittal on 29/08/14.
 */
public final class ParseConstants {
    //class name


    public static final String TYPE_VIDEO = "video";
    //field names
    public final static String KEY_USERID = "userId";
    public static final String KEY_SENDER_ID = "senderId";
    public static final String KEY_SENDER_NAME = "senderName";
    public static final String CLASS_MESSAGES = "Messages";
    public static final String KEY_RECIPIENT_IDS = "receipents";
    public static final String KEY_CREATED = "createdAt";
    final static String KEY_USERNAME = "username";
    final static String KEY_RELATION = "friends";
    final static String TYPE_IMAGE = "image";
    final static String KEY_FILE_TYPE = "fileType";
    final static String KEY_FILE = "FILE";
}
