package com.hcs.selfdestructingmessage;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by shivanshmittal on 01/09/14.
 */
public class InboxAdapter extends ArrayAdapter {


    Context mContext;
    List<ParseObject> mMessages;
    Date createdAt;

    public InboxAdapter(Context context, List<ParseObject> messages) {
        super(context, R.layout.inbox_row, messages);
        mContext = context;
        mMessages = messages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflate = LayoutInflater.from(mContext);
        ViewHolder holder;
        if(convertView==null) {
         convertView = View.inflate(mContext, R.layout.inbox_row,null);
            holder = new ViewHolder();
            holder.icon = (ImageView)  convertView.findViewById(R.id.icon1);
            holder.senderLabel = (TextView) convertView.findViewById(R.id.senderName);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            convertView.setTag(holder);

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(mMessages.get(position).getString(ParseConstants.KEY_FILE_TYPE).equals(ParseConstants.TYPE_IMAGE))
            holder.icon.setImageResource(R.drawable.ic_picture);
        else
        holder.icon.setImageResource(R.drawable.ic_video);

        holder.senderLabel.setText(mMessages.get(position).getString(ParseConstants.KEY_SENDER_NAME));
        createdAt = mMessages.get(position).getCreatedAt();
        long now = new Date().getTime();
        String time = DateUtils.getRelativeTimeSpanString(createdAt.getTime(),now,DateUtils.SECOND_IN_MILLIS).toString();
        holder.time.setText(time);
        return convertView;

    }

    static  class ViewHolder{
        ImageView icon;
        TextView senderLabel;
        TextView time;

    }


}
