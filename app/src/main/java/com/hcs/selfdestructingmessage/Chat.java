package com.hcs.selfdestructingmessage;

import android.app.ActionBar;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;


public class Chat extends ActionBarActivity {
    String recipientid;
    ListView list;
    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        list = (ListView)findViewById(R.id.list_inbox);
        message = (TextView) findViewById(R.id.chatMessage);
        ActionBar actionBar = getActionBar();
        recipientid=getIntent().getStringExtra(ParseConstants.KEY_RECIPIENT_IDS);
        actionBar.setTitle(recipientid);
        Bundle extras = getIntent().getExtras();
        Bitmap bmp = (Bitmap) extras.getParcelable("imagebitmap");
        Drawable icon = new BitmapDrawable(getResources(),bmp);
        actionBar.setIcon(icon);

        ImageView send = (ImageView) findViewById(R.id.send);
        send.setOnClickListener(mSendClickListener());

//        ImageView icon = (ImageView) getIntent().getIntExtra("image");

    }

    private View.OnClickListener mSendClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
                message.setText("");

            }
        };
    }

    private void sendMessage() {
        ArrayList<String> recipientsIDs = new ArrayList<String>();
        recipientsIDs.add(recipientid);
        ParseObject message = new ParseObject(ParseConstants.CLASS_MESSAGES);
        message.put(ParseConstants.KEY_SENDER_ID, ParseUser.getCurrentUser().getObjectId());
        message.put(ParseConstants.KEY_SENDER_NAME, ParseUser.getCurrentUser().getUsername());
        message.put(ParseConstants.KEY_RECIPIENT_IDS,recipientsIDs);
        message.put(ParseConstants.KEY_FILE_TYPE,"message");
        message.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null)
                {
                    Toast.makeText(Chat.this,"successfully sent",Toast.LENGTH_LONG).show();

                }
                else{
                    Log.e("chat",e.getMessage());
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
