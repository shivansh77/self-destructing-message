package com.hcs.selfdestructingmessage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by shivanshmittal on 29/08/14.
 */
public class FriendsFragment extends Fragment {

    final String TAG = FriendsFragment.class.getSimpleName();
    protected List<ParseUser> mUsers;
    protected ParseUser mUser = ParseUser.getCurrentUser();
    protected ParseRelation<ParseUser> relation = null;
    GridView friendsGrid;
    private AdapterView.OnItemClickListener gridClickListener;
    

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);
        friendsGrid = (GridView) rootView.findViewById(R.id.friendsGrid);
        friendsGrid.setEmptyView(rootView.findViewById(R.id.empty));
friendsGrid.setOnItemClickListener(gridClickListener);
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setProgressBarIndeterminateVisibility(true);
        relation = mUser.getRelation(ParseConstants.KEY_RELATION);
        ParseQuery<ParseUser> query = relation.getQuery();
        query.addAscendingOrder(ParseConstants.KEY_USERNAME);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> friends, ParseException e) {
                String[] friendNames = null;
                if (e == null) {
                    //success
                    friendNames = new String[friends.size()];
                    for (int i = 0; i < friends.size(); i++) {
                        friendNames[i] = friends.get(i).getUsername();
                        UserAdapter adapter = new UserAdapter(getActivity(), friends);
                        friendsGrid.setAdapter(adapter);

                        getActivity().setProgressBarIndeterminateVisibility(false);
                    }


                } else {
                    Log.e(TAG, e.getMessage());
                }


            }


        });
    }


}